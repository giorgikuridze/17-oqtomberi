package com.example.group11lecture4

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
        auth = FirebaseAuth.getInstance()
    }


    private fun init() {
        readData()
        logInButton.setOnClickListener {
            saveUserData()
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()) {
                login()

            } else {
                Toast.makeText(this, "Log In Failed", Toast.LENGTH_LONG).show()
            }

        }
        SignUpButton.setOnClickListener {
            val intent = Intent(this,ThirdActivity::class.java)
            startActivity(intent)
        }
    }
    private fun login(){
        auth.signInWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("login", "signInWithEmail:success")
                    val user = auth.currentUser
                    val intent = Intent(this,SecondActivity::class.java)
                    intent.putExtra("email",emailEditText.text.toString())
                    intent.putExtra("password",passwordEditText.text.toString())
                    startActivity(intent)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("login", "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()

                }

                // ...
            }


    }
    private fun saveUserData(){
                val sharedPreferences = getSharedPreferences("user_Data", Context.MODE_PRIVATE)
                val edit = sharedPreferences.edit()
                edit.putString("email",emailEditText.text.toString())
                edit.putString("password",passwordEditText.text.toString())
                edit.commit()

    }
    private fun readData(){
        val sharedPreferences = getSharedPreferences("user_Data", Context.MODE_PRIVATE)
        emailEditText.setText(sharedPreferences.getString("email",""))
        passwordEditText.setText(sharedPreferences.getString("password",""))
    }
}