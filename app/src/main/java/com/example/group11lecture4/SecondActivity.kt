package com.example.group11lecture4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()

    }
    private fun init(){
        Glide.with(this).load("https://images.unsplash.com/photo-1573196444192-cc9f26e94408?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(ImageView1)
        Glide.with(this).load("https://images.unsplash.com/photo-1572385008709-fc445242f7b3?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(ImageView2)
        Glide.with(this).load("https://images.unsplash.com/photo-1574274575744-7ee525dc51d7?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(ImageView3)
        Glide.with(this).load("https://images.unsplash.com/photo-1573764852047-592d46dabdf6?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(ImageView4)
        Glide.with(this).load("https://thispersondoesnotexist.com/image").into(profileImageView)
        Glide.with(this).load("https://images.unsplash.com/photo-1574423532599-284bee305305?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(coverImageView)
        // emailTextView.text = intent.extras?.getString("email","")
        // passwordTextView.text = intent.extras?.getString("password","")

        // Write a message to the database
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("user")

// Read from the database
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val value = dataSnapshot.getValue(UserModel::class.java)
                fistNameTextView.text=  value!!.firstName
                lastNameTextView.text=  value!!.lastName
                ageTextView.text = value!!.age.toString()
                Log.d("", "Value is: $value")
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("", "Failed to read value.", error.toException())
            }
        })

    }


}
